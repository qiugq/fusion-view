import request from '@/utils/request'

// 查询数据大屏模板分类列表
export function listClassification(query) {
  return request({
    url: '/datav/classification/list',
    method: 'get',
    params: query
  })
}

// 查询数据大屏模板分类详细
export function getClassification(id) {
  return request({
    url: '/datav/classification/' + id,
    method: 'get'
  })
}

// 新增数据大屏模板分类
export function addClassification(data) {
  return request({
    url: '/datav/classification',
    method: 'post',
    data: data
  })
}

// 修改数据大屏模板分类
export function updateClassification(data) {
  return request({
    url: '/datav/classification',
    method: 'put',
    data: data
  })
}

// 删除数据大屏模板分类
export function delClassification(id) {
  return request({
    url: '/datav/classification/' + id,
    method: 'delete'
  })
}

// 导出数据大屏模板分类
export function exportClassification(query) {
  return request({
    url: '/datav/classification/export',
    method: 'get',
    params: query
  })
}

// 下拉树
export function treeselect() {
  return request({
    url: '/datav/classification/treeselect',
    method: 'get'
  })
}