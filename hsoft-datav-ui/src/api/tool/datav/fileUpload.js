import request from '@/utils/request'

// 查询数据大屏文件上传映射列表
export function listUpload(query) {
    return request({
      url: '/datav/upload/list',
      method: 'get',
      params: query
    })
  }

// 查询数据大屏文件上传模板列表
export function fileTemplateList(query) {
    return request({
      url: '/datav/upload/template/list',
      method: 'get',
      params: query
    })
  }
  
  // 查询数据大屏文件上传映射详细
  export function getUpload(uploadId) {
    return request({
      url: '/datav/upload/' + uploadId,
      method: 'get'
    })
  }
  
  // 新增数据大屏文件上传映射
  export function addUpload(data) {
    return request({
      url: '/datav/upload',
      method: 'post',
      data: data
    })
  }
  
  // 修改数据大屏文件上传映射
  export function updateUpload(data) {
    return request({
      url: '/datav/upload',
      method: 'put',
      data: data
    })
  }
  
  // 删除数据大屏文件上传映射
  export function delUpload(uploadId) {
    return request({
      url: '/datav/upload/' + uploadId,
      method: 'delete'
    })
  }

//文件上传
export function fileUpload(data) {
    return request({
        url: '/datav/upload/fileUpload',
        method: 'post',
        data: data
      })
}

//视频上传
export function videoUpload(data) {
    return request({
        url: '/datav/upload/videoUpload',
        method: 'post',
        data: data
      })
}

// 删除视频
  export function videoDel(uploadId) {
    return request({
      url: '/datav/upload/videoDel/' + uploadId,
      method: 'delete'
    })
  }

  // 更新图片素材查看数、收藏数、创建数
  export function updateTemplate(data) {
    return request({
      url: '/datav/upload/update/template',
      method: 'post',
      data: data
    })
  }
