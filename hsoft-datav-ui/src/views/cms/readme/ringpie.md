# 环形图

## 一、配置

### 1、图层

选中该环形图组件，在操作界面右侧的“图层名称”处修改组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；
<viewer>
<img src="../image/ringpie-1.png" width="80%">
</viewer>

### 2、标题

选中该环形图组件，在操作界面右侧的“标题”处可修改环形图组件的标题，如下图。
<viewer>
<img src="../image/ringpie-2.png" width="80%">
</viewer>

- 标题：标题显示的内容，如果不想显示标题，则不填写；
          
- 副标题：副标题显示的内容，如果不想显示副标题，则不填写；

- 字体颜色：此设置修改标题颜色；
<viewer>
<img src="../image/ringpie-2-1.png" width="60%">
</viewer>

### 3、图例

选中该环形图组件，在操作界面右侧的“图例”处可修改环形图图例属性设置，如下图。
<viewer>
<img src="../image/ringpie-3.png" width="80%">
</viewer>

- 显示图例开关：该开关控制图例的显示与隐藏。若隐藏，效果如图所示；
  <viewer>
  <img src="../image/ringpie-3-2.png" width="40%">
  <img src="../image/ringpie-3-1.png" width="30%">
  </viewer>

- 图例样式：单选，分为「水平」「垂直」，如下图；
  <viewer>
  <img src="../image/ringpie-3-3.png" width="60%">
  </viewer>
  <viewer>
  <img src="../image/ringpie-3-4.png" width="60%">
  </viewer>

- 图例水平位置：单选，分为「居左」「居中」「居右」；

- 图例垂直位置：单选，分为「顶部」「中部」「底部」；

### 4、饼图

选中该环形图组件，在操作界面右侧的“饼图”处可修改环饼图组件有关饼图的属性设置，如下图。
<viewer>
<img src="../image/ringpie-4.png" width="80%">
</viewer>

- 内半径：环饼内侧半径，位置标注如下图；
  <viewer>
  <img src="../image/ringpie-4-1.png" width="60%">
  </viewer>

- 外半径：环饼外侧半径，位置标注如下图；
  <viewer>
  <img src="../image/ringpie-4-2.png" width="60%">
  </viewer>

- 内环线宽度：控制内部四截黄线距离宽度，位置标注如下图；
  <viewer>
  <img src="../image/ringpie-4-3.png" width="60%">
  </viewer>

### 5、引导线

选中该环饼图组件，在操作界面右侧的“引导线”处可修改环形图组件引导线有关的属性设置，如下图。
<viewer>
<img src="../image/ringpie-5.png" width="80%">
</viewer>

- 一段引导线长度：控制靠近环形图的第一段引导线长度，位置指示如下图；
  <viewer>
  <img src="../image/ringpie-5-1.png" width="60%">
  </viewer>

- 二段引导线长度：控制靠近标注的第二段引导线长度，位置指示如下图；
  <viewer>
  <img src="../image/ringpie-5-2.png" width="60%">
  </viewer>

### 6、动画

选中该组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：

- 弹跳

- 渐隐渐显

- 向右滑入

- 向左滑入

- 放缩

- 旋转

- 滚动

## 二、数据

静态数据格式：

```
  [
    {"name":"火车","value":20},
    {"name":"飞机","value":10},
    {"name":"客车","value":30},
    {"name":"轮渡","value":40}
  ]
```
