# 时间轴

## 配置

### 1、图层

选中该组件，在操作界面右侧的“图层名称”处修改组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理。

### 2、默认样式

选中该组件，在操作界面右侧的“默认样式”处可修改该时间轴的默认样式。

- 单位：时间轴上显示的单位，如下图：
  <viewer>
    <img src="../image/timelines-2-1.png" width="80%">
  </viewer> 

- 标签字体大小/颜色：控制时间轴上字体的字号/颜色；
  <viewer>
    <img src="../image/timelines-2-2.png" width="80%">
  </viewer> 

- 圆点大小/颜色：坐标轴上默认圆点的样式，如下图：
  <viewer>
    <img src="../image/timelines-2-3.png" width="80%">
  </viewer> 

- 时间轴颜色：轴位置如下图：
  <viewer>
    <img src="../image/timelines-2-4.png" width="80%">
  </viewer> 

- 按钮颜色：默认效果下按钮的颜色，按钮位置如下图：
  <viewer>
    <img src="../image/timelines-2-5.png" width="80%">
  </viewer> 

### 3、选中样式

选中该组件，在操作界面右侧的“选中样式”处可修改该时间轴的选中样式。

- 标签字体颜色：鼠标聚焦在时间轴上标签时文字的颜色设置；

- 圆点颜色：选中时间轴上某一坐标时，轴上圆点的颜色；
  <viewer>
    <img src="../image/timelines-3-1.png" width="80%">
  </viewer> 

- 边框颜色/大小：坐标轴上选中某一圆点时圆点边框的颜色/大小；

- 按钮颜色：选中状态下按钮的颜色；
  <viewer>
    <img src="../image/timelines-3-2.png" width="80%">
  </viewer> 

### 4、控制

选中该组件，在操作界面右侧的“控制”处可修改时间轴的动态事件配置。

- 自动播放开关：是否自动播放；

- 播放间隔：圆点转换的时间间隔；

- 显示播放开关：时间轴是否依顺序自动播放；

- 显示下一个开关：是否显示“上一个”开关；

- 显示上一个开关：是否显示“下一个”开关；
  <viewer>
    <img src="../image/timelines-2-5.png" width="80%">
  </viewer> 

### 5、绑定组件

选中该组件，在操作界面右侧的“绑定组件”处可修改参数名称、设置所要绑定的组件且可以绑定多个组件联动。此处以饼图联动为例：
* 设置参数名称，选定所要联动的组件，如下图：
  ><viewer>
  ><img src="../image/timelines-5-1.png" width="60%">
  ></viewer> 
* 绑定后，即可联动显示：
  ><viewer>
  ><img src="../image/timelines-5-2.png" width="60%">
  ></viewer> 
* 联动后，饼图传参为：
  ><viewer>
  ><img src="../image/timelines-5-3.png" width="60%">
  ></viewer> 

### 6、动画

选中该组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：

- 弹跳

- 渐隐渐显

- 向右滑入

- 向左滑入

- 放缩

- 旋转

- 滚动

